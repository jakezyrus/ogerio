package com.example.bl.converter;

import android.icu.text.DecimalFormat;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

public class temp extends AppCompatActivity {
    RadioButton r,r1;
    Button btn;
    TextView disp;
    EditText txt;
    int check;
    String t;
    double temp;
    public DecimalFormat two= new DecimalFormat("#0.00");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);
        btn=(Button) findViewById(R.id.button);
        txt=(EditText) findViewById(R.id.editText);
        disp=  (TextView) findViewById(R.id.textView);
        r1=(RadioButton) findViewById(R.id.radioButton);
        r=(RadioButton) findViewById(R.id.radioButton2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(r.isChecked())
                {
                    t = txt.getText().toString();
                    temp = Double.parseDouble(t)*1.8 + 32;
                    disp.setText(two.format(temp) + "F");

                }
                else if (r1.isChecked())
                {
                    t = txt.getText().toString();
                    temp = (Double.parseDouble(t)-32)*5/9;
                    disp.setText(two.format(temp) + "F");

                }

            }
        });

    }
}
